#include <gtest/gtest.h>
#include <fstream>


extern "C" {
#include "patched_array.h"
#include "matrix.h"
#include "deleter.h"
}

#define PATCHED_ARRAY_ROWS 5

TEST(PatchedArrayTest, InitTest) {
    PatchedArray *arr = init_array(1);

    ASSERT_EQ(arr->rows, 1);
    ASSERT_NE(arr, nullptr);

    free_array(arr);
}

TEST(PatchedArrayTest, PrintingBadInputTest) {
    PatchedArray array;
    const char *path = "example";
    int status = print_array_to_file(&array, nullptr);
    ASSERT_EQ(status, BAD_INPUT_ERROR);

    status = print_array_to_file(nullptr, path);
    ASSERT_EQ(status, BAD_INPUT_ERROR);
}

TEST(PatchedArrayTest, PrintingBadFileTest) {
    const char *bad_file_name = "\0";
    PatchedArray array;
    int status = print_array_to_file(&array, bad_file_name);
    ASSERT_EQ(status, FILE_ERROR);
}

TEST(PatchedArrayTest, AddingElemTest) {
    const int elem = 42;
    PatchedArray *array = init_array(PATCHED_ARRAY_ROWS);
    ASSERT_EQ(add_elem(array, elem, PATCHED_ARRAY_ROWS), nullptr);
    ASSERT_EQ(add_elem(nullptr, elem, PATCHED_ARRAY_ROWS - 1), nullptr);

    add_elem(array, elem, PATCHED_ARRAY_ROWS - 1);

    ASSERT_EQ(array->rows, PATCHED_ARRAY_ROWS);
    ASSERT_EQ(array->sizes[PATCHED_ARRAY_ROWS - 1], 1);
    ASSERT_EQ(array->mem_sizes[PATCHED_ARRAY_ROWS - 1], BUFFER);
    ASSERT_EQ(array->arr[PATCHED_ARRAY_ROWS - 1][0], elem);

    free_array(array);
}

TEST(PatchedArrayTest, PrintingFileTest) {
    Matrix *matrix = create_matrix_from_file("../hw-1/tests/data/matrix/good_one");
    PatchedArray *array = deleter(matrix);
    int status = print_array_to_file(array, "../hw-1/tests/data/patched_array/received_output");
    ASSERT_EQ(status, EXIT_SUCCESS);

    std::ifstream expected_file("../hw-1/tests/data/patched_array/expected_output");
    std::ifstream received_file("../hw-1/tests/data/patched_array/received_output");

    char rec_char = 0;
    char exp_char = 0;

    while (expected_file >> exp_char) {
        ASSERT_TRUE(static_cast<bool>(received_file >> rec_char));
        ASSERT_EQ(exp_char, rec_char);
    }

    ASSERT_FALSE(static_cast<bool>(received_file >> rec_char));
    free_array(array);
    free_matrix(matrix);
}
