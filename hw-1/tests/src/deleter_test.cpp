#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"
#include <gtest/gtest.h>

#include <fstream>

extern "C" {
#include "deleter.h"
#include "matrix.h"
#include "patched_array.h"
 }


TEST(DeleterTest, BadInputTest) {
    Matrix *matrix = nullptr;
    ASSERT_EQ(deleter(matrix), nullptr);
}

TEST(DeleterTest, WorkingTest) {
    Matrix *matrix = create_matrix_from_file("../hw-1/tests/data/matrix/good_one");

    PatchedArray *answer = deleter(matrix);
    print_array_to_file(answer, "../hw-1/tests/data/array/received_output");

    std::ifstream expected_file("../hw-1/tests/data/patched_array/expected_output");
    std::ifstream received_file("../hw-1/tests/data/patched_array/received_output");

    char rec_char = 0;
    char exp_char = 0;

    while (expected_file >> exp_char) {
        ASSERT_TRUE(static_cast<bool>(received_file >> rec_char));
        ASSERT_EQ(exp_char, rec_char);
    }

    ASSERT_FALSE(static_cast<bool>(received_file >> rec_char));

    free_array(answer);
    free_matrix(matrix);
}
