#include <gtest/gtest.h>

extern "C" {
#include "matrix.h"
}

#include "matrix_test.h"


TEST(MatrixTest, CreateByElementsTest) {
    Matrix *matrix = create_matrix(MATRIX_CREATION_ROWS, MATRIX_CREATION_COLS);
    ASSERT_NE(matrix, nullptr);
    free_matrix(matrix);
}

TEST(MatrixTest, BadInputCreationTest) {
    Matrix *matrix = create_matrix(0, MATRIX_CREATION_COLS);
    ASSERT_EQ(matrix, nullptr);

    matrix = create_matrix(MATRIX_CREATION_ROWS, 0);
    ASSERT_EQ(matrix, nullptr);
}

TEST(MatrixTest, BadInputCreationFromFileTest) {
    Matrix *matrix = create_matrix_from_file(nullptr);
    ASSERT_EQ(matrix, nullptr);
}

TEST(MatrixTest, BadFileCreationTest) {
    const char* bad_path = "\0";
    Matrix *matrix = create_matrix_from_file(bad_path);
    ASSERT_EQ(matrix, nullptr);
}

TEST(MatrixTest, BadDataFileCreationTest) {
    Matrix *matrix = create_matrix_from_file("../hw-1/tests/data/matrix/bad_format");
    ASSERT_EQ(matrix, nullptr);

    matrix = create_matrix_from_file("../hw-1/tests/data/matrix/bad_data");
    ASSERT_EQ(matrix, nullptr);
}

TEST(MatrixTest, MatrixFileCreationTest) {
    const ExpectedMatrix expected = {
            {{1, 2, 3}, {4, 5, 6}, {7, 8, 9},
             {10, 11, 12}, {13, 14, 15}},
            EXPECTED_MATRIX_ROWS,
            EXPECTED_MATRIX_COLS
    };

    Matrix *matrix = create_matrix_from_file("../hw-1/tests/data/matrix/good_one");
    ASSERT_EQ(matrix->rows, expected.rows);
    ASSERT_EQ(matrix->cols, expected.cols);

    for (size_t i = 0; i < matrix->rows; ++i) {
        for (size_t j = 0; j < matrix->cols; ++j) {
            ASSERT_EQ(matrix->mas[i][j], expected.mas[i][j]);
        }
    }

    free_matrix(matrix);
}
