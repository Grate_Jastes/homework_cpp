#ifndef HW_1_TESTS_INCLUDE_MATRIX_TEST_H_
#define HW_1_TESTS_INCLUDE_MATRIX_TEST_H_

#define MATRIX_CREATION_ROWS 5
#define MATRIX_CREATION_COLS 5

#define EXPECTED_MATRIX_ROWS 5
#define EXPECTED_MATRIX_COLS 3

typedef struct {
    int mas[EXPECTED_MATRIX_ROWS][EXPECTED_MATRIX_COLS];
    size_t rows;
    size_t cols;
} ExpectedMatrix;


#endif  // HW_1_TESTS_INCLUDE_MATRIX_TEST_H_
