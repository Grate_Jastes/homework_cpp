#ifndef HW_1_INCLUDE_MATRIX_H_
#define HW_1_INCLUDE_MATRIX_H_

#include <stddef.h>

#define ONE_VALUE_READING_SUCCESS 1
#define TWO_VALUES_READING_SUCCESS 2

typedef struct {
    int **mas;
    size_t rows;
    size_t cols;
} Matrix;

Matrix* create_matrix_from_file(const char* path_to_file);
Matrix* create_matrix(size_t rows, size_t cols);
int print_matrix_to_file(const Matrix *matrix, const char* path_to_file);
void free_matrix(Matrix* matrix);


#endif  // HW_1_INCLUDE_MATRIX_H_
