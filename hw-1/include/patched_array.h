#ifndef HW_1_INCLUDE_PATCHED_ARRAY_H_
#define HW_1_INCLUDE_PATCHED_ARRAY_H_

#include <stddef.h>

#define BUFFER 8

#define BAD_INPUT_ERROR -1
#define FILE_ERROR -2

typedef struct {
    int **arr;
    size_t *sizes;
    size_t *mem_sizes;
    size_t rows;
} PatchedArray;

PatchedArray *init_array(size_t rows);
PatchedArray *add_elem(PatchedArray *array, int elem, size_t row);
int print_array_to_file(const PatchedArray *array, const char* path_to_file);
void free_array(PatchedArray *array);

#endif  // HW_1_INCLUDE_PATCHED_ARRAY_H_
