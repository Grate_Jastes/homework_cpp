#ifndef HW_1_INCLUDE_DELETER_H_
#define HW_1_INCLUDE_DELETER_H_

#include "matrix.h"
#include "patched_array.h"

PatchedArray *deleter(const Matrix *matrix);

#endif  // HW_1_INCLUDE_DELETER_H_
