#include <stdlib.h>

#include "deleter.h"
#include "matrix.h"
#include "patched_array.h"

PatchedArray *deleter(const Matrix *matrix) {
    if (matrix == NULL)
        return NULL;

    PatchedArray *answer = init_array(matrix->rows);
    if (answer == NULL)
        return NULL;

    for (size_t i = 0; i < matrix->rows; ++i) {
        for (size_t j = 0; j < matrix->cols; ++j) {
            if (!(matrix->mas[i][j] % 2)) {
                if (add_elem(answer, matrix->mas[i][j], i) == NULL) {
                    free_array(answer);
                    return NULL;
                }
            }
        }
    }

    return answer;
}
