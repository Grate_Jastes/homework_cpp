#include <stddef.h>
#include <stdlib.h>

#include "matrix.h"
#include "patched_array.h"
#include "deleter.h"


int main(int argc, char** argv) {
    Matrix *A = create_matrix_from_file(argv[1]);
    if (A == NULL)
        return EXIT_FAILURE;

    PatchedArray *answer = deleter(A);
    int status = print_array_to_file(answer, "stdout");

    free_array(answer);
    free_matrix(A);

    if (status != EXIT_SUCCESS)
        return EXIT_FAILURE;
    else
        return EXIT_SUCCESS;
}
