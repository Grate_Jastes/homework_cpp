#include <stdlib.h>
#include <stdio.h>

#include "matrix.h"

Matrix *create_matrix(size_t rows, size_t cols) {
    if (rows == 0 || cols == 0) {
        return NULL;
    }

    Matrix *new_matrix = malloc(sizeof(Matrix));
    if (new_matrix == NULL) {
        return NULL;
    }

    new_matrix->mas = (int**)malloc(rows  *sizeof(int*));
    if (new_matrix->mas == NULL) {
        free(new_matrix);
        return NULL;
    }

    new_matrix->rows = rows;
    new_matrix->cols = cols;

    for (size_t i = 0; i < rows; ++i) {
        new_matrix->mas[i] = (int*)(calloc(cols, sizeof(int)));
        if (new_matrix->mas[i] == NULL) {
            new_matrix->rows = i;
            free_matrix(new_matrix);
            return NULL;
        }
    }
    return new_matrix;
}

Matrix *create_matrix_from_file(const char *path_to_file) {
    if (path_to_file == NULL) {
        return NULL;
    }

    FILE *source_file = fopen(path_to_file, "r");
    if (source_file == NULL) {
        return NULL;
    }

    size_t rows = 0;
    size_t cols = 0;
    if (fscanf(source_file, "%zu %zu", &rows, &cols) != TWO_VALUES_READING_SUCCESS) {
        fclose(source_file);
        return NULL;
    }

    Matrix *matrix = create_matrix(rows, cols);
    if (matrix == NULL) {
        fclose(source_file);
        return NULL;
    }

    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < cols; ++j) {
            int status = fscanf(source_file, "%d ", &matrix->mas[i][j]);
            if (status != ONE_VALUE_READING_SUCCESS) {
                fclose(source_file);
                free_matrix(matrix);
                return NULL;
            }
        }
    }

    fclose(source_file);
    return matrix;
}

void free_matrix(Matrix *matrix) {
    for (size_t i = 0; i < matrix->rows; ++i) {
        free(matrix->mas[i]);
    }
    free(matrix->mas);
    free(matrix);
}
//
// int print_matrix_to_file(const Matrix *matrix, const char *path_to_file) {
//    if (matrix == NULL) {
//        return -1;
//    }
//    if (path_to_file == NULL) {
//        return -2;
//    }
//
//    FILE *dest_file = NULL;
//    if ((dest_file = fopen(path_to_file, "w")) == NULL) {
//        return -3;
//    }
//
//    int status = fprintf(dest_file, "%zu %zu\n", matrix->rows, matrix->cols);
//    if (status < 1) {
//        fclose(dest_file);
//        return -4;
//    }
//
//    for (size_t i = 0; i < matrix->rows; ++i) {
//        for (size_t j = 0; j < matrix->cols; ++j) {
//            status = fprintf(dest_file, "%d ", matrix->mas[i][j]);
//            if (status < 1) {
//                fclose(dest_file);
//                return -4;
//            }
//        }
//        fprintf(dest_file, "\n");
//    }
//
//    fclose(dest_file);
//    return EXIT_SUCCESS;
//}
