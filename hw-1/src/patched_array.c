#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "patched_array.h"

PatchedArray *init_array(const size_t rows) {
    PatchedArray *result = calloc(1, sizeof(PatchedArray));
    if (result == NULL)
        return NULL;

    result->arr = calloc(rows, sizeof(int*));
    if (result->arr == NULL) {
        free_array(result);
        return NULL;
    }


    result->sizes = calloc(rows, sizeof(size_t));
    if (result->sizes == NULL) {
        free_array(result);
        return NULL;
    }

    result->mem_sizes = calloc(rows, sizeof(size_t));
    if (result->mem_sizes == NULL) {
        free_array(result);
        return NULL;
    }

    result->rows = rows;

    return result;
}
void free_array(PatchedArray *array) {
    free(array->sizes);
    for (size_t i = 0; i < array->rows; ++i) {
        free(array->arr[i]);
    }
    free(array->arr);
    free(array->mem_sizes);
    free(array);
}
PatchedArray *add_elem(PatchedArray *array, const int elem, const size_t row) {
    if (array == NULL)
        return NULL;

    if (row >= array->rows)
        return NULL;

    if (array->sizes[row] == 0) {
        int* tmp = realloc(array->arr[row], BUFFER * sizeof(int));
        if (tmp) {
            array->arr[row] = tmp;
        } else {
            return NULL;
        }

        array->mem_sizes[row] = BUFFER;
    } else if (array->sizes[row] == array->mem_sizes[row]) {
        int* tmp = realloc(array->arr[row], array->mem_sizes[row] * 2);
        if (tmp) {
            array->arr[row] = tmp;
        } else {
            return NULL;
        }

        array->mem_sizes[row] *= 2;
    }

    array->arr[row][array->sizes[row]] = elem;
    ++array->sizes[row];

    return array;
}

int print_array_to_file(const PatchedArray *array, const char* path_to_file) {
    if (array == NULL) {
        return BAD_INPUT_ERROR;
    }

    if (path_to_file == NULL) {
        return BAD_INPUT_ERROR;
    }

    FILE *dest_file = NULL;
    if (!strcmp(path_to_file, "stdout")) {
        dest_file = stdout;
    } else {
        if ((dest_file = fopen(path_to_file, "w")) == NULL) {
            return FILE_ERROR;
        }
    }

    fprintf(dest_file, "rows_sizes: ");
    for (size_t i = 0; i < array->rows; ++i) {
        fprintf(dest_file, "%zu ", array->sizes[i]);
    }
    putc('\n', dest_file);

    for (size_t i = 0; i < array->rows; ++i) {
        for (size_t j = 0; j < array->sizes[i]; ++j) {
            fprintf(dest_file, "%d ", array->arr[i][j]);
        }
        putc('\n', dest_file);
    }

    fclose(dest_file);
    return EXIT_SUCCESS;
}
